#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const { Team } = require('pubsweet-server/src/models')

const makeTeam = async type => {
  const names = {
    productionEditor: 'Production Editor',
  }

  logger.info(`Create ${names[type]} team`)

  const team = new Team({
    global: true,
    members: [],
    name: names[type],
    teamType: type,
  })

  await team.save()
  logger.info(`${names[type]} team successfully created`)
}

const seed = async () => {
  logger.info('### RUNNING GLOBAL TEAMS SEED SCRIPTS ###')
  logger.info('=> Checking if global teams exist...')

  try {
    const teams = await Team.findByField({ global: true })

    const productionEditorTeam = teams.find(
      t => t.teamType === 'productionEditor',
    )

    if (productionEditorTeam) {
      logger.info('All global teams found, exiting...')
    } else {
      logger.warn('No Production Editor team found')
      await makeTeam('productionEditor')
    }
  } catch (err) {
    logger.warn('No global teams found')

    await makeTeam('productionEditor')
  }

  logger.info('Team seed successfully finished')
}

seed()
